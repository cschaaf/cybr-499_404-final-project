import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JLayeredPane;

/**
 * CYBR 499 / 404 Final Project - Extraction Module
 * Chris Schaaf
 * 
 * 
 * Extract Class
 * Uses the Embed class algorithm in reverse to extract the secret data embedded in a cover image
 * Requires the same password to generate a key used to embed the data otherwise only gibberish will be extracted
 * Choose a modified image file > choose the number of bits per color channel 
 *     (must match with what was embedded but nothing stops you from trying all three until a valid message is found) >
 *     enter the same password used to embed data > click EXTRACT
 * 
 * @author cscha
 *
 */
public class Extract {

	
	// commented values not currently in use but left in for portability
    private final int EXTRACT_STYLE;
    //private final String PASSWORD;
    private final long KEY;
    //private final String MESSAGE;
    private final File IMAGE_FILE;
    private final Random RNG = new Random();
    // parent frame/pane needed if an error message is needed to be generated within this class
    //private final JLayeredPane PARENT;
    private StringBuilder extractedBinaryMessage = new StringBuilder();
    private StringBuilder reconstructedMessage = new StringBuilder();
    
	
    public Extract(JLayeredPane parent, File file, int extractStyle, String password) {
    	//this.PARENT = parent;
    	this.IMAGE_FILE = file;
    	this.EXTRACT_STYLE = extractStyle;
    	//this.PASSWORD = password;
    	//this.MESSAGE = message;
    	this.KEY = this.passwordToKey(password);
    	this.RNG.setSeed(this.KEY);
    }


    public String run() {
    	
    	try {
            BufferedImage image = ImageIO.read(this.IMAGE_FILE);
            WritableRaster raster = image.getRaster();
            final int UPPERXBOUND = raster.getWidth();
            final int UPPERYBOUND = raster.getHeight();
            final int NUMPIXELS = UPPERXBOUND * UPPERYBOUND;
            int x = 0;
            int y = 0;
            int[] pixel = new int[3];
            // initialize 2d array representing pixels in the image to 0, as a pixel is modified, change that pixel location in the array to 1
            int[][] pixelList = new int[UPPERYBOUND][UPPERXBOUND];
            for(int i = 0; i < UPPERYBOUND; i++) {
                for(int j = 0; j < UPPERXBOUND; j++) {
                    pixelList[i][j] = 0;
                }
            }

            // main extraction loop
            int channelBitVal = 0;
            int characterIndex = 0;
            while(characterIndex < NUMPIXELS) {
                // choose a pixel to modify randomly based on a designated seed, if that pixel has already been modified, look at the next pixel to the right and down in
                // that order until an unmodified pixel is found
                x = this.RNG.nextInt(UPPERXBOUND);
                y = this.RNG.nextInt(UPPERYBOUND);
                while(isPixelUsed(pixelList, x, y)) {
                    x += 1;
                    if(x >= UPPERXBOUND) {
                        x = 0;
                        y += 1;
                        if(y >= UPPERYBOUND) {
                            y = 0;
                        }
                    }
                }
                raster.getPixel(x, y, pixel);
                // set pixel as modified
                pixelList[y][x] = 1;

                // Begin Extraction
                // 1 bit per color channel
                if(this.EXTRACT_STYLE == 1) {
                	// RED CHANNEL
                    channelBitVal = pixel[0] & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // GREEN CHANNEL
                    channelBitVal = pixel[1] & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // BLUE CHANNEL
                    channelBitVal = pixel[2] & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                }
                // 2 bits per color channel
                if(this.EXTRACT_STYLE == 2) {
                	// RED CHANNEL
                	// 0000 000x
                	channelBitVal = pixel[0] & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // 0000 00x0
                    channelBitVal = pixel[0] >> 1 & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // GREEN CHANNEL
                    // 0000 000x
                    channelBitVal = pixel[1] & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // 0000 00x0
                    channelBitVal = pixel[1] >> 1 & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // BLUE CHANNEL
                    // 0000 000x
                    channelBitVal = pixel[2] & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // 0000 00x0
                    channelBitVal = pixel[2] >> 1 & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                }
                // 3 bits per color channel
                if(this.EXTRACT_STYLE == 3) {
                	// RED CHANNEL
                	// 0000 000x
                	channelBitVal = pixel[0] & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // 0000 00x0
                    channelBitVal = pixel[0] >> 1 & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // 0000 0x00
                    channelBitVal = pixel[0] >> 2 & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // GREEN CHANNEL
                    // 0000 000x
                    channelBitVal = pixel[1] & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // 0000 00x0
                    channelBitVal = pixel[1] >> 1 & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // 0000 0x00
                    channelBitVal = pixel[1] >> 2 & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // BLUE CHANNEL
                    // 0000 000x
                    channelBitVal = pixel[2] & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // 0000 00x0
                    channelBitVal = pixel[2] >> 1 & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                    // 0000 0x00
                    channelBitVal = pixel[2] >> 2 & 1;
                    extractedBinaryMessage.append(channelBitVal);
                    characterIndex++;
                }
            }
            
            // convert back to readable text
            String subString = "";
            int charByte = 0;		// integer ascii value of a substring made of 8 binary digits
            for(int i = 0; i + 8 < extractedBinaryMessage.length(); i += 8) {
                subString = extractedBinaryMessage.substring(i, i + 8);
                charByte = Integer.parseInt(subString, 2);
                // eliminate characters that don't fall in the typical english ascii characters from 0-127
                if(charByte < 128) {
                    reconstructedMessage.append((char)charByte);
                }
            }
            
        }
        catch(IOException e) {e.printStackTrace();}
    	
        return reconstructedMessage.toString();

    }
    
    
    /**
     * convert a string password into a long key used as a random number generator seed
     * 
     * @param in - shared password key
     * @return - long value generated from string password
     */
    private long passwordToKey(String in) {
        long key = 1;
        for(int i = 1; i < in.length(); i++) {
            int temp1 = in.charAt(i);
            int temp2 = in.charAt(i-1);
            int temp3 = temp1 * temp2;
            key *= temp3;
        }
        return key;
    }


    /**
     * check if a particular pixel in the image has been modified during message embedding
     * 
     * @param pixelList - 2d array of integers representing pixels in an image, 0 if the pixel is original, 1 if it had been modified
     * @param x - column to check if pixel has already been modified
     * @param y - row to check if pixel has already been modified
     * @return - boolean, true if the indicated pixel has been modified, false otherwise
     */
    private boolean isPixelUsed(int[][] pixelList, int x, int y) {
        if(pixelList[y][x] == 1) return true;
        else return false;
    }
    
}
