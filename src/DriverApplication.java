import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ItemEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JLayeredPane;
import java.awt.Image;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JTextField;
import java.awt.Dimension;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextArea;


/**
 * Desktop Application Driver using Java Swing libraries
 * Constructed using Eclipse WindowBuilder plugin to create, place, modify, and generate gui elements
 * element code modified and edited by Chris Schaaf
 * All event handling coded by Chris Schaaf
 * 
 * @author cscha
 */
public class DriverApplication {

	
	// Base Frame
	private JFrame frmSteggsoarus;
	// Analyze Elements
	private JTextField analyzeFilePathTextField;
	private JTextField bitPerByte1TextField;
	private JTextField bitPerByte2TextField;
	private JTextField bitPerByte3TextField;
	private JTextField imageWidthTextField;
	private JTextField imageHeightTextField;
	private JTextField numPixelsTextField;
	// Compare Elements
	private JTextField compareCoverImageFilePath = new JTextField();
	private JTextField compareModifiedImageFilePath = new JTextField();
	private JLabel compareCoverImageView = new JLabel("");
	private JLabel compareModifiedImageView = new JLabel("");
	private JLabel compareComparisonImageView = new JLabel("");
	// Embed Elements
	private JTextField embedCoverImageFilePath = new JTextField();
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField embedPasswordTextField;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextField embedMessageFilePath = new JTextField();;
	private JLabel embedCoverImageView = new JLabel("");
	private JButton embedChooseMessageFileButton = new JButton("Choose Message File");
	private JTextArea embedMessageEntryTextArea = new JTextArea();
	// Extract Elements
	private JTextField extractModifiedImageFilePath;
	private JTextField extractPasswordTextField;
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private JTextArea extractMessageTextArea = new JTextArea();
	private JLabel extractModifiedImageView = new JLabel("");
	// Shared Variables
	private int embedStyle = 0;
	private int extractStyle = 0;
	//private int messageStyle = 0;


	/**
	* Launch the application.
	*/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DriverApplication window = new DriverApplication();
					window.frmSteggsoarus.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Create the application.
	 */
	public DriverApplication() {
		initialize();
	}

	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		// Panel 1 - Embed
		JLayeredPane embedLayeredPane = new JLayeredPane();
		embedLayeredPane.setVisible(false);
		embedLayeredPane.setEnabled(false);
		// Panel 2 - Extract
		JLayeredPane extractLayeredPane = new JLayeredPane();
		extractLayeredPane.setEnabled(false);
		extractLayeredPane.setVisible(false);
		// Panel 3 - Analyzer
		JLayeredPane analyzeLayeredPane = new JLayeredPane();
		analyzeLayeredPane.setRequestFocusEnabled(false);
		analyzeLayeredPane.setVisible(false);
		analyzeLayeredPane.setEnabled(false);
		// Panel 4 - Compare
		JLayeredPane compareLayeredPane = new JLayeredPane();
		compareLayeredPane.setEnabled(false);
		compareLayeredPane.setVisible(false);
		// Base Frame
		frmSteggsoarus = new JFrame();
		frmSteggsoarus.setTitle("Stegg-0-Soar-Us");
		frmSteggsoarus.setBounds(100, 100, 1050, 650);
		frmSteggsoarus.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Menu Bar
		JMenuBar menuBar = new JMenuBar();
		frmSteggsoarus.setJMenuBar(menuBar);
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Exit");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		JMenu mnNewMenu_1 = new JMenu("Help");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Help");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				File aboutFile = new File("Help.txt");
				StringBuilder prompt = new StringBuilder();
				String temp = "";
				try {
					BufferedReader reader = new BufferedReader(new FileReader(aboutFile));
					boolean reading = true;
					while(reading) {
						temp = reader.readLine();
						if(temp != null) {
							prompt.append(temp);
							prompt.append("\n");
						}
						else reading = false;
					}
					reader.close();
				} catch(IOException e) {e.printStackTrace();}
				JScrollPane scroll = new JScrollPane();				
				JOptionPane.showMessageDialog(scroll, prompt);		
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("About");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				File aboutFile = new File("About.txt");
				StringBuilder prompt = new StringBuilder();
				String temp = "";
				try {
					BufferedReader reader = new BufferedReader(new FileReader(aboutFile));
					boolean reading = true;
					while(reading) {
						temp = reader.readLine();
						if(temp != null) {
							prompt.append(temp);
							prompt.append("\n");
						}
						else reading = false;
					}
					reader.close();
				} catch(IOException e) {e.printStackTrace();}
				JScrollPane scroll = new JScrollPane();				
				JOptionPane.showMessageDialog(scroll, prompt);	
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_2);
		SpringLayout springLayout = new SpringLayout();
		frmSteggsoarus.getContentPane().setLayout(springLayout);
		
		JPanel panel = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel, 0, SpringLayout.NORTH, frmSteggsoarus.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panel, 0, SpringLayout.WEST, frmSteggsoarus.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel, 68, SpringLayout.NORTH, frmSteggsoarus.getContentPane());
		frmSteggsoarus.getContentPane().add(panel, BorderLayout.NORTH);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);
		
		JLabel selectModuleLabel = new JLabel("Select Program Module");
		selectModuleLabel.setToolTipText("Select what you want to do");
		sl_panel.putConstraint(SpringLayout.NORTH, selectModuleLabel, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, selectModuleLabel, 10, SpringLayout.WEST, panel);
		panel.add(selectModuleLabel);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				int moduleIndex = comboBox.getSelectedIndex();
				boolean embedBool = false;
				boolean extractBool = false;
				boolean analyzeBool = false;
				boolean compareBool = false;
				if(moduleIndex == 1) {
					embedBool = true;
					extractBool = false;
					analyzeBool = false;
					compareBool = false;
				}
				if(moduleIndex == 2) {
					embedBool = false;
					extractBool = true;
					analyzeBool = false;
					compareBool = false;
				}
				if(moduleIndex == 3) {
					embedBool = false;
					extractBool = false;
					analyzeBool = true;
					compareBool = false;
				}
				else if(moduleIndex == 4) {
					embedBool = false;
					extractBool = false;
					analyzeBool = false;
					compareBool = true;
				}
				embedLayeredPane.setEnabled(embedBool);
				embedLayeredPane.setVisible(embedBool);
				extractLayeredPane.setEnabled(extractBool);
				extractLayeredPane.setVisible(extractBool);
				analyzeLayeredPane.setEnabled(analyzeBool);
				analyzeLayeredPane.setVisible(analyzeBool);
				compareLayeredPane.setEnabled(compareBool);
				compareLayeredPane.setVisible(compareBool);
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "Embed", "Extract", "Analyze", "Compare"}));
		sl_panel.putConstraint(SpringLayout.NORTH, comboBox, 6, SpringLayout.SOUTH, selectModuleLabel);
		sl_panel.putConstraint(SpringLayout.WEST, comboBox, 0, SpringLayout.WEST, selectModuleLabel);
		sl_panel.putConstraint(SpringLayout.EAST, comboBox, 0, SpringLayout.EAST, selectModuleLabel);
		panel.add(comboBox);
		
		JLayeredPane layeredPane = new JLayeredPane();
		springLayout.putConstraint(SpringLayout.NORTH, layeredPane, 6, SpringLayout.SOUTH, panel);
		springLayout.putConstraint(SpringLayout.WEST, layeredPane, 10, SpringLayout.WEST, frmSteggsoarus.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, layeredPane, -10, SpringLayout.SOUTH, frmSteggsoarus.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, layeredPane, -10, SpringLayout.EAST, frmSteggsoarus.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panel, 0, SpringLayout.EAST, layeredPane);
		frmSteggsoarus.getContentPane().add(layeredPane);
		
		// embedLayeredPane
		embedLayeredPane.setRequestFocusEnabled(false);
		layeredPane.setLayer(embedLayeredPane, 10);
		embedLayeredPane.setBounds(0, 0, 1015, 505);
		layeredPane.add(embedLayeredPane);
		
		JLabel lblEmbedDataIn = new JLabel("Embed Data In Cover Image");
		lblEmbedDataIn.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmbedDataIn.setBounds(0, 10, 1015, 23);
		embedLayeredPane.add(lblEmbedDataIn);
		
		JButton embedChooseCoverImageButton = new JButton("Choose Cover Image File");
		embedChooseCoverImageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser();
				String filePath = "";
				File file;
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "bmp", "png");
		        chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(compareLayeredPane);
				if(returnVal == 0) {
					file = chooser.getSelectedFile();
					filePath = file.getPath();
					embedCoverImageFilePath.setText(filePath);
					try {
				         BufferedImage image = ImageIO.read(file);
				         Image scaledImage = image.getScaledInstance(embedCoverImageView.getWidth(), embedCoverImageView.getHeight(), Image.SCALE_SMOOTH);
				         ImageIcon icon = new ImageIcon(scaledImage);
				         embedCoverImageView.setIcon(icon);
				    } catch (IOException e) {e.printStackTrace();}
				}
			}
		});
		embedChooseCoverImageButton.setToolTipText("");
		embedChooseCoverImageButton.setBounds(50, 50, 200, 23);
		embedLayeredPane.add(embedChooseCoverImageButton);
		
		// embedCoverImageFilePath
		embedCoverImageFilePath.setEditable(false);
		embedCoverImageFilePath.setColumns(10);
		embedCoverImageFilePath.setBounds(275, 50, 575, 23);
		embedLayeredPane.add(embedCoverImageFilePath);
		
		JLabel embedEmbedStyleLabel = new JLabel("Bits per Color Channel");
		embedEmbedStyleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		embedEmbedStyleLabel.setToolTipText("Number of bits to use per color channel for embedding data.");
		embedEmbedStyleLabel.setBounds(15, 100, 160, 14);
		embedLayeredPane.add(embedEmbedStyleLabel);
		
		JRadioButton embed1bpccRadioButton = new JRadioButton("1 bit per color channel");
		embed1bpccRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				embedStyle = 1;
			}
		});
		
		buttonGroup.add(embed1bpccRadioButton);
		embed1bpccRadioButton.setBounds(15, 120, 160, 23);
		embedLayeredPane.add(embed1bpccRadioButton);
		
		JRadioButton embed2bpccRadioButton = new JRadioButton("2 bits per color channel");
		embed2bpccRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				embedStyle = 2;
			}
		});
		buttonGroup.add(embed2bpccRadioButton);
		embed2bpccRadioButton.setBounds(15, 140, 160, 23);
		embedLayeredPane.add(embed2bpccRadioButton);
		
		JRadioButton embed3bpccRadioButton = new JRadioButton("3 bits per color channel");
		embed3bpccRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				embedStyle = 3;
			}
		});
		buttonGroup.add(embed3bpccRadioButton);
		embed3bpccRadioButton.setBounds(15, 160, 160, 23);
		embedLayeredPane.add(embed3bpccRadioButton);
		
		JLabel embedPasswordLabel = new JLabel("Enter Password");
		embedPasswordLabel.setHorizontalAlignment(SwingConstants.CENTER);
		embedPasswordLabel.setBounds(225, 135, 100, 14);
		embedLayeredPane.add(embedPasswordLabel);
		
		embedPasswordTextField = new JTextField();
		embedPasswordTextField.setColumns(10);
		embedPasswordTextField.setBounds(335, 130, 575, 23);
		embedLayeredPane.add(embedPasswordTextField);
		
		JLabel embedMessageStyleLabel = new JLabel("Message Type");
		embedMessageStyleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		embedMessageStyleLabel.setBounds(15, 225, 85, 14);
		embedLayeredPane.add(embedMessageStyleLabel);
		
		JRadioButton embedMessageTypeFileRadioButton = new JRadioButton(".txt File");
		embedMessageTypeFileRadioButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				boolean buttonBool = false;
				boolean textBool = false;
				
				if(arg0.getStateChange() == ItemEvent.SELECTED) {
					buttonBool = true;
					textBool = true;
				}
				embedChooseMessageFileButton.setEnabled(buttonBool);
				embedMessageFilePath.setEnabled(textBool);
			}
		});
		
		embedMessageTypeFileRadioButton.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup_1.add(embedMessageTypeFileRadioButton);
		embedMessageTypeFileRadioButton.setBounds(15, 245, 95, 23);
		embedLayeredPane.add(embedMessageTypeFileRadioButton);
		
		// embedChooseMessageFileButton
		embedChooseMessageFileButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser();
				String filePath = "";
				File file;
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt");
		        chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(compareLayeredPane);
				if(returnVal == 0) {
					file = chooser.getSelectedFile();
					filePath = file.getPath();
					embedMessageFilePath.setText(filePath);
					StringBuilder message = new StringBuilder();
					try {
						BufferedReader reader = new BufferedReader(new FileReader(file));
						String temp = "";
						boolean reading = true;
						while(reading) {
							temp = reader.readLine();
							if(temp != null) {
								message.append(temp);
								message.append("\n");
							}
							else {reading = false;}
						}
						reader.close();
						embedMessageEntryTextArea.setText(message.toString());
					}
					catch(IOException ex) {ex.printStackTrace();}
				}
			}
		});
		
		embedChooseMessageFileButton.setEnabled(false);
		embedChooseMessageFileButton.setBounds(110, 245, 170, 23);
		embedLayeredPane.add(embedChooseMessageFileButton);
		
		embedMessageFilePath.setEditable(false);
		embedMessageFilePath.setColumns(10);
		embedMessageFilePath.setBounds(290, 245, 350, 23);
		embedLayeredPane.add(embedMessageFilePath);
		
		JRadioButton embedMessageTypeEntryRadioButton = new JRadioButton("Text Entry");
		embedMessageTypeEntryRadioButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				boolean textEdit = false;
				
				if(arg0.getStateChange() == ItemEvent.SELECTED) {
					textEdit = true;
					embedMessageEntryTextArea.setText("");
				}
				embedMessageEntryTextArea.setEditable(textEdit);
			}
		});
		
		embedMessageTypeEntryRadioButton.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup_1.add(embedMessageTypeEntryRadioButton);
		embedMessageTypeEntryRadioButton.setBounds(15, 265, 95, 23);
		embedLayeredPane.add(embedMessageTypeEntryRadioButton);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(15, 300, 625, 190);
		embedLayeredPane.add(scrollPane_1);
		embedMessageEntryTextArea.setEditable(false);
		scrollPane_1.setViewportView(embedMessageEntryTextArea);
		
		JLabel embedCoverImageViewLabel = new JLabel("Cover Image Preview");
		embedCoverImageViewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		embedCoverImageViewLabel.setBounds(775, 180, 125, 14);
		embedLayeredPane.add(embedCoverImageViewLabel);
		
		// embedCoverImageView
		embedCoverImageView.setBounds(685, 200, 315, 290);
		embedLayeredPane.add(embedCoverImageView);
		
		JButton embedRun = new JButton("EMBED");
		embedRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = null;
				int imageWidth = 0;
				int imageHeight = 0;
				int numPixels = 0;
				// create file from filepath text field for cover image file
				if(embedCoverImageFilePath.getText() != "") {
					file = new File(embedCoverImageFilePath.getText());
				}
				// get number of pixels in image
				try {
					BufferedImage image = ImageIO.read(file);
					imageWidth = image.getWidth();
					imageHeight = image.getHeight();
					numPixels = imageWidth * imageHeight;
				}
				catch(IOException e) {e.printStackTrace();}
				
				// create string from password text field
				String password = embedPasswordTextField.getText();
				// create string from message text area
				String message = embedMessageEntryTextArea.getText();
				
				// create new embed object with above arguments including parent frame
				if(file != null && embedStyle != 0 && password != "" && message != "") {
					
					int messageLength = message.length();
			    	double maxEmbedSize = (((double)embedStyle * 3.00) * (double)numPixels) /8.00;
			    	if(messageLength > (int)maxEmbedSize) {
			    		String errorPrompt = "Message is too large for this embed style!";
			    		JOptionPane.showMessageDialog(embedLayeredPane, errorPrompt);
			    	}
			    	else {
						Embed eb = new Embed(embedLayeredPane, file, embedStyle, password, message);
						eb.run();
			    	}
				}
			}
		});
		
		embedRun.setBounds(875, 50, 100, 50);
		embedLayeredPane.add(embedRun);
		
		// extractLayeredPane
		extractLayeredPane.setBounds(0, 0, 1015, 505);
		layeredPane.add(extractLayeredPane);
		
		JLabel lblExtractDataFrom = new JLabel("Extract Data From Modified Image");
		lblExtractDataFrom.setHorizontalAlignment(SwingConstants.CENTER);
		lblExtractDataFrom.setBounds(0, 10, 1015, 23);
		extractLayeredPane.add(lblExtractDataFrom);
		
		JButton extractChooseModefiedImageButton_1 = new JButton("Choose Modified Image File");
		extractChooseModefiedImageButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser();
				String filePath = "";
				File file;
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "bmp", "png");
		        chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(compareLayeredPane);
				if(returnVal == 0) {
					file = chooser.getSelectedFile();
					filePath = file.getPath();
					extractModifiedImageFilePath.setText(filePath);
					try {
				         BufferedImage image = ImageIO.read(file);
				         Image scaledImage = image.getScaledInstance(extractModifiedImageView.getWidth(), extractModifiedImageView.getHeight(), Image.SCALE_SMOOTH);
				         ImageIcon icon = new ImageIcon(scaledImage);
				         extractModifiedImageView.setIcon(icon);
				    } catch (IOException e) {e.printStackTrace();}
				}
			}
		});
		
		extractChooseModefiedImageButton_1.setToolTipText("");
		extractChooseModefiedImageButton_1.setBounds(50, 50, 200, 23);
		extractLayeredPane.add(extractChooseModefiedImageButton_1);
		
		extractModifiedImageFilePath = new JTextField();
		extractModifiedImageFilePath.setEditable(false);
		extractModifiedImageFilePath.setColumns(10);
		extractModifiedImageFilePath.setBounds(275, 50, 575, 23);
		extractLayeredPane.add(extractModifiedImageFilePath);
		
		JLabel extractEmbedStyleLabel = new JLabel("Bits per Color Channel");
		extractEmbedStyleLabel.setToolTipText("Number of bits to use per color channel for embedding data.");
		extractEmbedStyleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		extractEmbedStyleLabel.setBounds(15, 100, 160, 14);
		extractLayeredPane.add(extractEmbedStyleLabel);
		
		JRadioButton extract1bpccRadioButton = new JRadioButton("1 bit per color channel");
		extract1bpccRadioButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				extractStyle = 1;
			}
		});
		
		buttonGroup_2.add(extract1bpccRadioButton);
		extract1bpccRadioButton.setBounds(15, 120, 160, 23);
		extractLayeredPane.add(extract1bpccRadioButton);
		
		JRadioButton extract2bpccRadioButton = new JRadioButton("2 bits per color channel");
		extract2bpccRadioButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				extractStyle = 2;
			}
		});
		
		buttonGroup_2.add(extract2bpccRadioButton);
		extract2bpccRadioButton.setBounds(15, 140, 160, 23);
		extractLayeredPane.add(extract2bpccRadioButton);
		
		JRadioButton extract3bpccRadioButton = new JRadioButton("3 bits per color channel");
		extract3bpccRadioButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				extractStyle = 3;
			}
		});
		
		buttonGroup_2.add(extract3bpccRadioButton);
		extract3bpccRadioButton.setBounds(15, 160, 160, 23);
		extractLayeredPane.add(extract3bpccRadioButton);
		
		JLabel extractPasswordLabel = new JLabel("Enter Password");
		extractPasswordLabel.setHorizontalAlignment(SwingConstants.CENTER);
		extractPasswordLabel.setBounds(225, 135, 100, 14);
		extractLayeredPane.add(extractPasswordLabel);
		
		extractPasswordTextField = new JTextField();
		extractPasswordTextField.setColumns(10);
		extractPasswordTextField.setBounds(335, 130, 575, 23);
		extractLayeredPane.add(extractPasswordTextField);
		
		JLabel extractMessageLabel = new JLabel("Extracted Message");
		extractMessageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		extractMessageLabel.setBounds(255, 210, 125, 14);
		extractLayeredPane.add(extractMessageLabel);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(15, 240, 623, 250);
		extractLayeredPane.add(scrollPane_2);
		
		// extractMessageTextArea
		scrollPane_2.setViewportView(extractMessageTextArea);
		extractMessageTextArea.setEditable(false);
		
		JLabel extractModifiedImageViewLabel = new JLabel("Modified Image Preview");
		extractModifiedImageViewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		extractModifiedImageViewLabel.setBounds(775, 180, 135, 14);
		extractLayeredPane.add(extractModifiedImageViewLabel);
		
		// extractModifiedImageView
		extractModifiedImageView.setBounds(685, 200, 315, 290);
		extractLayeredPane.add(extractModifiedImageView);
		
		JButton extractRun = new JButton("EXTRACT");
		extractRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//get image file
				File file = new File(extractModifiedImageFilePath.getText());  
				//get password
				String password = extractPasswordTextField.getText();
				//construct extract class
				Extract ex = new Extract(extractLayeredPane, file, extractStyle, password);
				//create string, run extract which returns message string
				String extractedMessage = ex.run();
				//set extractMessageTextArea text to returned string
				extractMessageTextArea.setText(extractedMessage);
			}
		});
		
		extractRun.setBounds(875, 50, 100, 50);
		extractLayeredPane.add(extractRun);
		analyzeFilePathTextField = new JTextField();
		bitPerByte1TextField = new JTextField();
		bitPerByte2TextField = new JTextField();
		bitPerByte3TextField = new JTextField();
		JLabel imageView = new JLabel("");
		imageWidthTextField = new JTextField();
		imageHeightTextField = new JTextField();
		numPixelsTextField = new JTextField();
		layeredPane.setLayer(analyzeLayeredPane, 1);
		analyzeLayeredPane.setBounds(0, 0, 1015, 505);
		layeredPane.add(analyzeLayeredPane);
		
		JLabel lblNewLabel = new JLabel("Analyze Cover Image");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 10, 1015, 23);
		analyzeLayeredPane.add(lblNewLabel);
		
		JButton chooseAnalyzeImageButton = new JButton("Choose Image File");
		chooseAnalyzeImageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser();
				String filePath = "";
				File file;
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "bmp", "png");
		        chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(analyzeLayeredPane);
				if(returnVal == 0) {
					file = chooser.getSelectedFile();
					filePath = file.getPath();
					analyzeFilePathTextField.setText(filePath);	
					try {
				         BufferedImage image = ImageIO.read(file);
				         ImageIcon icon = new ImageIcon(image);
				         imageView.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
				         imageView.setIcon(icon);  
				    } catch (IOException e) {e.printStackTrace();}	
				}
			}
		});
		
		chooseAnalyzeImageButton.setToolTipText("Choose a potential image file to get character limites for embedding");
		chooseAnalyzeImageButton.setBounds(50, 50, 139, 23);
		analyzeLayeredPane.add(chooseAnalyzeImageButton);
		
		// analyzer chosen file text field
		analyzeFilePathTextField.setEditable(false);
		analyzeFilePathTextField.setBounds(225, 50, 575, 23);
		analyzeLayeredPane.add(analyzeFilePathTextField);
		analyzeFilePathTextField.setColumns(10);
		
		JButton analyzeButton = new JButton("ANALYZE");
		analyzeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(analyzeFilePathTextField.getText() != "") {
					String filePath = analyzeFilePathTextField.getText();
					File file = new File(filePath);
					Analyze al = new Analyze(analyzeLayeredPane, file);
					int[] results = al.run();
					
					bitPerByte1TextField.setText(Integer.toString(results[0]));
					bitPerByte2TextField.setText(Integer.toString(results[1]));
					bitPerByte3TextField.setText(Integer.toString(results[2]));	
					
					int width = al.getImageWidth();
					int height = al.getImageHeight();
					imageWidthTextField.setText(Integer.toString(width));
					imageHeightTextField.setText(Integer.toString(height));
					numPixelsTextField.setText(Integer.toString(width * height));
				}
			}
		});
		
		analyzeButton.setBounds(50, 115, 100, 45);
		analyzeLayeredPane.add(analyzeButton);
		
		JLabel imageWidthLabel = new JLabel("Width:  ");
		imageWidthLabel.setHorizontalAlignment(SwingConstants.LEFT);
		imageWidthLabel.setBounds(15, 185, 45, 14);
		analyzeLayeredPane.add(imageWidthLabel);
		
		// imageWidthTextField
		imageWidthTextField.setBounds(55, 185, 75, 20);
		analyzeLayeredPane.add(imageWidthTextField);
		imageWidthTextField.setColumns(10);
		
		JLabel imageHeightLabel = new JLabel("Height:  ");
		imageHeightLabel.setHorizontalAlignment(SwingConstants.LEFT);
		imageHeightLabel.setBounds(140, 185, 50, 14);
		analyzeLayeredPane.add(imageHeightLabel);
		
		// imageHeightTextField
		imageHeightTextField.setBounds(185, 185, 75, 20);
		analyzeLayeredPane.add(imageHeightTextField);
		imageHeightTextField.setColumns(10);
		
		JLabel numPixelsLabel = new JLabel("Pixels:  ");
		numPixelsLabel.setHorizontalAlignment(SwingConstants.LEFT);
		numPixelsLabel.setBounds(270, 185, 45, 14);
		analyzeLayeredPane.add(numPixelsLabel);
		
		// numPixelsTextField
		numPixelsTextField.setBounds(310, 185, 100, 20);
		analyzeLayeredPane.add(numPixelsTextField);
		numPixelsTextField.setColumns(10);
		
		JLabel resultLabel = new JLabel("Max number of characters in message");
		resultLabel.setHorizontalAlignment(SwingConstants.CENTER);
		resultLabel.setBounds(50, 235, 225, 14);
		analyzeLayeredPane.add(resultLabel);
		
		JLabel bitPerByte1Label = new JLabel("1 bit per color channel / 3 bits per byte:  ");
		bitPerByte1Label.setBounds(15, 275, 200, 14);
		analyzeLayeredPane.add(bitPerByte1Label);
		
		// 1 bit per color channel result field
		bitPerByte1TextField.setBounds(225, 275, 150, 20);
		analyzeLayeredPane.add(bitPerByte1TextField);
		bitPerByte1TextField.setColumns(10);
		
		JLabel bitPerByte2Label = new JLabel("2 bits per color channel / 6 bits per byte:  ");
		bitPerByte2Label.setBounds(15, 325, 201, 14);
		analyzeLayeredPane.add(bitPerByte2Label);
		
		// 2 bits per color channel result field
		bitPerByte2TextField.setBounds(225, 325, 150, 20);
		analyzeLayeredPane.add(bitPerByte2TextField);
		bitPerByte2TextField.setColumns(10);
		
		JLabel bitPerByte3Label = new JLabel("3 bits per color channel / 9 bits per byte:  ");
		bitPerByte3Label.setBounds(15, 375, 201, 14);
		analyzeLayeredPane.add(bitPerByte3Label);
		
		// 3 bits per color channel result field
		bitPerByte3TextField.setBounds(225, 375, 150, 20);
		analyzeLayeredPane.add(bitPerByte3TextField);
		bitPerByte3TextField.setColumns(10);
		
		JLabel imageViewLabel = new JLabel("Chosen Image");
		imageViewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		imageViewLabel.setBounds(675, 100, 85, 14);
		analyzeLayeredPane.add(imageViewLabel);
		
		// image view scroll window
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(425, 125, 575, 365);
		analyzeLayeredPane.add(scrollPane);
		scrollPane.setViewportView(imageView);
				compareLayeredPane.setBounds(0, 0, 1014, 505);
				layeredPane.add(compareLayeredPane);
				
				JLabel compareTitleLabel = new JLabel("Compare Cover and Modified Images");
				compareTitleLabel.setHorizontalAlignment(SwingConstants.CENTER);
				compareTitleLabel.setBounds(0, 10, 1015, 23);
				compareLayeredPane.add(compareTitleLabel);
				
				JButton compareChooseCoverImageButton = new JButton("Choose Cover Image File");
				compareChooseCoverImageButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						JFileChooser chooser = new JFileChooser();
						String filePath = "";
						File file;
						FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "bmp", "png");
				        chooser.setFileFilter(filter);
						int returnVal = chooser.showOpenDialog(compareLayeredPane);
						if(returnVal == 0) {
							file = chooser.getSelectedFile();
							filePath = file.getPath();
							compareCoverImageFilePath.setText(filePath);
							try {
						         BufferedImage image = ImageIO.read(file);
						         Image scaledImage = image.getScaledInstance(compareCoverImageView.getWidth(), compareCoverImageView.getHeight(), Image.SCALE_SMOOTH);
						         ImageIcon icon = new ImageIcon(scaledImage);
						         compareCoverImageView.setIcon(icon);
						    } catch (IOException e) {e.printStackTrace();}
						}
					}
				});
				
				compareChooseCoverImageButton.setToolTipText("");
				compareChooseCoverImageButton.setBounds(50, 50, 200, 23);
				compareLayeredPane.add(compareChooseCoverImageButton);
				
				compareCoverImageFilePath.setEditable(false);
				compareCoverImageFilePath.setColumns(10);
				compareCoverImageFilePath.setBounds(275, 50, 575, 23);
				compareLayeredPane.add(compareCoverImageFilePath);
				
				JButton compareChooseModifiedImageButton = new JButton("Choose Modified Image File");
				compareChooseModifiedImageButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						JFileChooser chooser = new JFileChooser();
						String filePath = "";
						File file;
						FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "bmp", "png");
				        chooser.setFileFilter(filter);
						int returnVal = chooser.showOpenDialog(compareLayeredPane);
						if(returnVal == 0) {
							file = chooser.getSelectedFile();
							filePath = file.getPath();
							compareModifiedImageFilePath.setText(filePath);
							try {
						         BufferedImage image = ImageIO.read(file);
						         Image scaledImage = image.getScaledInstance(compareModifiedImageView.getWidth(), compareModifiedImageView.getHeight(), Image.SCALE_SMOOTH);
						         ImageIcon icon = new ImageIcon(scaledImage);
						         compareModifiedImageView.setIcon(icon);
						    } catch (IOException e) {e.printStackTrace();}
						}
					}
				});
				
				compareChooseModifiedImageButton.setToolTipText("Choose a potential image file to get character limites for embedding");
				compareChooseModifiedImageButton.setBounds(50, 100, 200, 23);
				compareLayeredPane.add(compareChooseModifiedImageButton);
				
				compareModifiedImageFilePath.setEditable(false);
				compareModifiedImageFilePath.setColumns(10);
				compareModifiedImageFilePath.setBounds(275, 100, 575, 23);
				compareLayeredPane.add(compareModifiedImageFilePath);
				
				JButton compareRunButton = new JButton("COMPARE");
				compareRunButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						if(compareCoverImageFilePath.getText() != "" && compareModifiedImageFilePath.getText() != "") {
							
							File coverImageFile = new File(compareCoverImageFilePath.getText());
							File modifiedImageFile = new File(compareModifiedImageFilePath.getText());
							Compare cp = new Compare(compareLayeredPane, coverImageFile, modifiedImageFile);
							File comparisonImageFile = cp.run();
							
							try {
								BufferedImage comparisonImage = ImageIO.read(comparisonImageFile);
								Image scaledImage = comparisonImage.getScaledInstance(compareComparisonImageView.getWidth(), compareComparisonImageView.getHeight(), Image.SCALE_SMOOTH);
						        ImageIcon icon = new ImageIcon(scaledImage);
						        compareComparisonImageView.setIcon(icon);
							}
							catch(IOException e) {e.printStackTrace();}
						}
					}
				});
				
				compareRunButton.setBounds(875, 60, 100, 50);
				compareLayeredPane.add(compareRunButton);
				
				JLabel compareCoverImageViewLabel = new JLabel("Cover Image");
				compareCoverImageViewLabel.setHorizontalAlignment(SwingConstants.CENTER);
				compareCoverImageViewLabel.setBounds(200, 150, 75, 14);
				compareLayeredPane.add(compareCoverImageViewLabel);
				compareCoverImageView.setBounds(15, 170, 315, 335);
				compareLayeredPane.add(compareCoverImageView);
				
				JLabel compareModifiedImageViewLabel = new JLabel("Modified Image");
				compareModifiedImageViewLabel.setHorizontalAlignment(SwingConstants.CENTER);
				compareModifiedImageViewLabel.setBounds(455, 150, 100, 14);
				compareLayeredPane.add(compareModifiedImageViewLabel);
				
				compareModifiedImageView.setBounds(345, 170, 315, 335);
				compareLayeredPane.add(compareModifiedImageView);
				
				JLabel compareComparisonImageViewLabel = new JLabel("Comparison Image");
				compareComparisonImageViewLabel.setHorizontalAlignment(SwingConstants.CENTER);
				compareComparisonImageViewLabel.setBounds(715, 150, 115, 14);
				compareLayeredPane.add(compareComparisonImageViewLabel);
				
				compareComparisonImageView.setBounds(675, 170, 315, 335);
				compareLayeredPane.add(compareComparisonImageView);
	}
}
