import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.JLayeredPane;


/**
 * CYBR 499 / 404 - Embedding program
 * Chris Schaaf
 * 
 * 
 * Selects a cover image to embed a message in
 * Takes a password or key that will also be used/required to extract an embedded message from a modified image
 * Allows user to select whether they wish to use a .txt file containing a message, or to directly enter the message to be embedded
 *   NOTE:  there are hard caps to the message size depending on the cover image size.  
 *   Number of characters allowed for the algorithm to work is:  
 *     total number of pixels * 3/8 for 1 bit per channel least significant bit steganography,
 *     total number of pixels * 6/8 for 2 bits per color channel
 *     total number of pixels * 9/8 for 3 bits per color channel (using 3 bits per channel can easily be detected even visually at times)
 * Embeds that message in the selected cover image
 * Save that as a new image as a .png file in order to avoid tricky .jpg compression issues losing embedded data
 *   (NOTE:  it is possible to use .jpg as the finished file but its more involved than I'm prepared to address at this moment.  The embedding must be done
 *      AFTER the lossy compression is done otherwise you will likely lose embedded data to said lossy compression) 
 */
public class Embed {


	// commented values not currently in use but left in for portability
    private final int EMBED_STYLE;
    //private final String PASSWORD;
    private final long KEY;
    //private final String MESSAGE;
    private final String BINARY_MESSAGE;
    private final int BINARY_MESSAGE_LENGTH;
    private final File IMAGE_FILE;
    private final Random RNG = new Random();
    // parent frame/pane needed if an error message is needed to be generated within this class
    //private final JLayeredPane PARENT;

    
    /**
     * Redesigned to be used with DriverApplication class using Java Swing GUI elements
     * 
     * @param parent - parent panel, not currently used but is needed for any error prompts or popup windows within this class
     * @param file - cover image file selected in the application, read into memory in this class, and used to create a new image with data embedded in it
     * @param embedStyle - number of bits to embed per color channel
     * @param password - String password used to generate a key used as a random seed for random object
     * @param message - message to embed in cover image
     */
    public Embed(JLayeredPane parent, File file, int embedStyle, String password, String message) {
    	//this.PARENT = parent;
    	this.IMAGE_FILE = file;
    	this.EMBED_STYLE = embedStyle;
    	//this.PASSWORD = password;
    	//this.MESSAGE = message;
    	this.KEY = this.passwordToKey(password);
    	this.RNG.setSeed(this.KEY);
    	this.BINARY_MESSAGE = this.messageToBinary(message);
    	this.BINARY_MESSAGE_LENGTH = this.BINARY_MESSAGE.length();
    }


   
    public void run() {
    	
        try {
            BufferedImage image = ImageIO.read(this.IMAGE_FILE);
            WritableRaster raster = image.getRaster();
            final int UPPERXBOUND = raster.getWidth();
            final int UPPERYBOUND = raster.getHeight();
            int x = 0;
            int y = 0;
            int[] pixel = new int[3];
            // initialize 2d array representing pixels in the image to 0, as a pixel is modified, change that pixel location in the array to 1
            int[][] pixelList = new int[UPPERYBOUND][UPPERXBOUND];
            for(int i = 0; i < UPPERYBOUND; i++) {
                for(int j = 0; j < UPPERXBOUND; j++) {
                    pixelList[i][j] = 0;
                }
            }

            // main embedding loop
            int channelBitVal = 0;
            int characterIndex = 0;
            while(characterIndex < this.BINARY_MESSAGE_LENGTH) {
                // choose a pixel to modify randomly based on a designated seed, if that pixel has already been modified, look at the next pixel to the right and down in
                // that order until an unmodified pixel is found
                x = this.RNG.nextInt(UPPERXBOUND);
                y = this.RNG.nextInt(UPPERYBOUND);
                while(isPixelUsed(pixelList, x, y)) {
                    x += 1;
                    if(x >= UPPERXBOUND) {
                        x = 0;
                        y += 1;
                        if(y >= UPPERYBOUND) {
                            y = 0;
                        }
                    }
                }
                raster.getPixel(x, y, pixel);
                // set pixel as modified
                pixelList[y][x] = 1;
                
                // start embedding message
                // embed 1 bit per color channel
                if(this.EMBED_STYLE == 1) {
                	try {
                		// RED CHANNEL
                		// 0000 000x
                        channelBitVal = (char)(pixel[0] & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[0] + 1 > 255) {pixel[0] -= 1;}
                        	else pixel[0] += 1;
                        }
                        characterIndex += 1;
                        // GREEN CHANNEL
                        // 0000 000x
                        if(characterIndex < BINARY_MESSAGE_LENGTH) {
                            channelBitVal = (char)(pixel[1] & 1);
                            if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                            	if(pixel[1] + 1 > 255) {pixel[1] -= 1;}
                            	else pixel[1] += 1;
                            }
                            characterIndex += 1;
                        }
                        // BLUE CHANNEL
                        // 0000 000x
                        if(characterIndex < BINARY_MESSAGE_LENGTH) {
                            channelBitVal = (char)(pixel[2] & 1);
                            if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                            	if(pixel[2] + 1 > 255) {pixel[2] -= 1;}
                            	else pixel[2] += 1;
                            }
                            characterIndex += 1;
                        }
                        //adjust for white values so modifying values don't go above the allowed 255 color value
                        //used for actual applications but commented out for this exercise
                         for(int i = 0; i < 3; i++) {
                             if(pixel[i] > 255) pixel[i] -= 2;
                         }
                        raster.setPixel(x, y, pixel);
                	}
                	catch(StringIndexOutOfBoundsException e) {
                		// ran out of characters in message to embed
                		raster.setPixel(x, y, pixel);
                		break;
                	}
                }        
                
                // embed 2 bits per color channel
                else if(this.EMBED_STYLE == 2) {
                	try {
                		// RED CHANNEL
                		// 0000 000x
                    	channelBitVal = (char)(pixel[0] & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[0] + 1 > 255) {pixel[0] -= 1;}
                        	else pixel[0] += 1;
                        }
                        characterIndex += 1;
                    	// 0000 00x0
                    	channelBitVal = (char)(pixel[0] >> 1 & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                            if(pixel[0] + 2 > 255) {pixel[0] -= 2;}
                            else {pixel[0] += 2;}
                        }
                        characterIndex += 1;
                    	// GREEN CHANNEL
                        // 0000 000x
                        channelBitVal = (char)(pixel[1] & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[1] + 1 > 255) {pixel[1] -= 1;}
                        	else pixel[1] += 1;
                        }
                        characterIndex += 1;
                    	// 0000 00x0
                    	channelBitVal = (char)(pixel[1] >> 1 & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                            if(pixel[1] + 2 > 255) {pixel[1] -= 2;}
                            else {pixel[1] += 2;}
                        }
                        characterIndex += 1;
                    	// BLUE CHANNEL
                        // 0000 000x
                        channelBitVal = (char)(pixel[2] & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[2] + 1 > 255) {pixel[2] -= 1;}
                        	else pixel[2] += 1;
                        }
                        characterIndex += 1;
                    	// 0000 00x0
                    	channelBitVal = (char)(pixel[2] >> 1 & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                            if(pixel[2] + 2 > 255) {pixel[2] -= 2;}
                            else {pixel[2] += 2;}
                        }
                        characterIndex += 1;
                        raster.setPixel(x, y, pixel);
                	}
                	catch(StringIndexOutOfBoundsException e) {
                		// ran out of characters in message to embed
                        raster.setPixel(x, y, pixel);
                		break;
                	}
                }
                
                // embed 3 bits per color channel
                else if(this.EMBED_STYLE == 3) {
                	try {
                		// RED CHANNEL
                		// 0000 000x
                    	channelBitVal = (char)(pixel[0] & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[0] + 1 > 255) {pixel[0] -= 1;}
                        	else pixel[0] += 1;
                        }
                        characterIndex += 1;
                    	// 0000 00x0
                    	channelBitVal = (char)(pixel[0] >> 1 & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                            if(pixel[0] + 2 > 255) {pixel[0] -= 2;}
                            else {pixel[0] += 2;}
                        }
                        characterIndex += 1;
                        // 0000 0x00
                        channelBitVal = (char)(pixel[0] >> 2 & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[0] + 4 > 255) {pixel[0] -= 4;}
                        	else pixel[0] += 4;
                        }
                        characterIndex += 1;
                    	// GREEN CHANNEL
                        // 0000 000x
                        channelBitVal = (char)(pixel[1] & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[1] + 1 > 255) {pixel[1] -= 1;}
                        	else pixel[1] += 1;
                        }
                        characterIndex += 1;
                    	// 0000 00x0
                    	channelBitVal = (char)(pixel[1] >> 1 & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                            if(pixel[1] + 2 > 255) {pixel[1] -= 2;}
                            else {pixel[1] += 2;}
                        }
                        characterIndex += 1;
                        // 0000 0x00
                        channelBitVal = (char)(pixel[1] >> 2 & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[1] + 4 > 255) {pixel[1] -= 4;}
                        	else pixel[1] += 4;
                        }
                        characterIndex += 1;
                    	// BLUE CHANNEL
                        // 0000 000x
                        channelBitVal = (char)(pixel[2] & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[2] + 1 > 255) {pixel[2] -= 1;}
                        	else pixel[2] += 1;
                        }
                        characterIndex += 1;
                    	// 0000 00x0
                    	channelBitVal = (char)(pixel[2] >> 1 & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                            if(pixel[2] + 2 > 255) {pixel[2] -= 2;}
                            else {pixel[2] += 2;}
                        }
                        characterIndex += 1;
                        // 0000 0x00
                        channelBitVal = (char)(pixel[2] >> 2 & 1);
                        if(channelBitVal != Integer.parseInt(this.BINARY_MESSAGE.substring(characterIndex, characterIndex + 1))) {
                        	if(pixel[2] + 4 > 255) {pixel[2] -= 4;}
                        	else pixel[2] += 4;
                        }
                        characterIndex += 1;
                        
                        raster.setPixel(x, y, pixel);
                	}
                	catch(StringIndexOutOfBoundsException e) {
                		// ran out of characters in message to embed
                        raster.setPixel(x, y, pixel);
                		break;
                	}
                }
            }
            File steggoedfile = new File("stegoImage.png");
            ImageIO.write(image, "png", steggoedfile);
        }
        catch(IOException e) {e.printStackTrace();}
    }


    /**
     * convert a string password into a long key used as a random number generator seed
     * 
     * @param in - shared password key
     * @return - long value generated from string password
     */
    private long passwordToKey(String in) {
        long key = 1;
        for(int i = 1; i < in.length(); i++) {
            int temp1 = in.charAt(i);
            int temp2 = in.charAt(i-1);
            int temp3 = temp1 * temp2;
            key *= temp3;
        }
        return key;
    }


    /**
     * Convert a string message into its binary byte equivalent for each character
     * 
     * @param message - message to be embedded
     * @return - string message converted to binary bytes
     */
    private String messageToBinary(String message) {
        StringBuilder binaryStringBuilder = new StringBuilder();
        int charVal;
        for(int i = 0; i < message.length(); i++) {
            charVal = message.charAt(i);
            binaryStringBuilder.append(String.format("%8s", Integer.toBinaryString(charVal)));
        }
        for(int i = 0; i < binaryStringBuilder.length(); i++) {
            if(binaryStringBuilder.charAt(i) == (char)32) {
                binaryStringBuilder.setCharAt(i, (char)48);
            }
        }
        return binaryStringBuilder.toString();
    }


    /**
     * check if a particular pixel in the image has been modified during message embedding
     * 
     * @param pixelList - 2d array of integers representing pixels in an image, 0 if the pixel is original, 1 if it had been modified
     * @param x - column to check if pixel has already been modified
     * @param y - row to check if pixel has already been modified
     * @return - boolean, true if the indicated pixel has been modified, false otherwise
     */
    private boolean isPixelUsed(int[][] pixelList, int x, int y) {
        if(pixelList[y][x] == 1) return true;
        else return false;
    } 
}
