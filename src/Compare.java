import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.JLayeredPane;


/**
 * CYBR 499 / 404 Final Project
 * Chris Schaaf
 * 
 * Subtractive comparison between an unmodified cover image and the modified image with steganographic message embedded
 * Used to identify pixels that have been modified using steganography when comparing a modified image with original cover image
 */
public class Compare {


    private final File ORIGINAL_IMAGE_FILE;
    private final File MODIFIED_IMAGE_FILE;
    private final JLayeredPane PARENT_PANE;


    /**
     * Constructor
     * @param parent - parent pane, in case popup messages need to be generated in this class
     * @param originalFile - original cover image file
     * @param modifiedFile - steganographically modified image file
     */
    public Compare(JLayeredPane parent, File originalFile, File modifiedFile) {
        this.PARENT_PANE = parent;
        this.ORIGINAL_IMAGE_FILE = originalFile;
        this.MODIFIED_IMAGE_FILE = modifiedFile;
    }


    /**
     * Run the subtractive comparison, create a new image and set its pixels to the values obtained 
     * by subtracting the rgb values from the modified and cover images
     * 
     * @return - a new image file where pixels where the original and modified images have the same values are pure black, and pixels that are different or modified will show up as the difference in color
     */
    public File run() {
        
    	File comparisonFile = null;
    	
		try {
			BufferedImage coverImage = ImageIO.read(this.ORIGINAL_IMAGE_FILE);
			BufferedImage modifiedImage = ImageIO.read(this.MODIFIED_IMAGE_FILE);
			BufferedImage comparisonImage = ImageIO.read(this.MODIFIED_IMAGE_FILE);
			int width = coverImage.getWidth();
			int height = coverImage.getHeight();
			int[] coverPixel = new int[3];
			int[] modifiedPixel = new int[3];
			int[] comparisonPixel = new int[3];
			for(int y = 0; y < height; y++) {
				for(int x = 0; x < width; x++) {
					coverImage.getRaster().getPixel(x, y, coverPixel);
					modifiedImage.getRaster().getPixel(x, y, modifiedPixel);
					comparisonPixel[0] = coverPixel[0] - modifiedPixel[0];
					comparisonPixel[1] = coverPixel[1] - modifiedPixel[1];
					comparisonPixel[2] = coverPixel[2] - modifiedPixel[2];
					comparisonImage.getRaster().setPixel(x, y, comparisonPixel);
				}
			}
			String fileName = "Comparison Image.png";
			comparisonFile = new File(fileName);
			ImageIO.write(comparisonImage, "png", comparisonFile);
		} 
		catch(IOException e) {e.printStackTrace();}
		
		return comparisonFile;
    }
    
    
    public File getOriginalFile() {
    	return this.ORIGINAL_IMAGE_FILE;
    }
    
    
    public File getModifiedFile() {
    	return this.MODIFIED_IMAGE_FILE;
    }
    
    
    public JLayeredPane getParent() {
    	return this.PARENT_PANE;
    } 
}
