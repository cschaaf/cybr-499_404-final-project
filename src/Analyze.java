import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.JLayeredPane;


/**
 * CYBR 499 / 404 Final Project
 * Chris Schaaf
 * 
 * Read a potential cover image and display the theorhetical maximum amount of data that can be embedded using different embedding strategies
 * Stragegies covered in this program based on least significant bit using 1, 2, or 3 bits per color channel per pixel, more than 2 bits is already a stretch for avoiding
 * detection but left 3 bits per channel in for demonstration purposes.  Also displays the potential number of bits per pixel able to be embedded using each strategy.
 * 
 * UPDATE:  added functionality to work with DriverApplication for a desktop gui application
 *     uses the 1 argument constructor and runWithGui() method 
 */
public class Analyze {

    
    private final JLayeredPane PARENT_PANE;
    private BufferedImage image;
    private int numPixels;
    private int imageHeight;
    private int imageWidth;
    
    
    /**
     * Constructor
     * 
     * @param parent - parent JPane, in case popup messages need to be generated here
     * @param file - image file to read and analyze
     */
    public Analyze(JLayeredPane parent, File file) {
    	this.PARENT_PANE = parent;
    	try {
    		this.image = ImageIO.read(file);
    		this.imageHeight = this.image.getHeight();
    		this.imageWidth = this.image.getWidth();
    		this.numPixels = this.imageHeight * this.imageWidth;
    	}
    	catch(IOException e) {}
    }

    
    /**
     * Called in DriverApplication to analyze an image for character embedding limits
     * 
     * @return - int[] of size 3 with character limits for embedding data with 1, 2, and 3 bits per color channel
     */
    public int[] run() {
    	
            this.numPixels = this.image.getHeight() * this.image.getWidth();
            double numChars1 = (double)this.numPixels * ((double)3/(double)8);
            double numChars2 = (double)this.numPixels * ((double)6/(double)8);
            double numChars3 = (double)this.numPixels * ((double)9/(double)8);
            
            int[] results = {(int)numChars1, (int)numChars2, (int)numChars3};
            return results;
    }
    
    
    public int getImageWidth() {
    	return this.imageWidth;
    }
    
    
    public int getImageHeight() {
    	return this.imageHeight;
    }
    
    
    public JLayeredPane getParentPane() {
    	return this.PARENT_PANE;
    }
}
